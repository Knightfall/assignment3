import sys
import re
from Scraper import Director
from IO import IO
from Formatter import Formatter


class Controller(object):

    def __init__(self):
        self.my_cmd = None
        self.my_scraper = Director()
        self.my_formatter = Formatter()
        self.full_dex = []
        self.local_dex = []
        self.response = ''
        self.default_path = 'F:\CourseMaterial(DO-NOT-DELETE)\
        Year4\PR301\PythonShit\pokedex\Pokedex'
        self.defaultName = "test01.txt"
        self.my_IO = IO()

    def get_scraper(self):
        return self.my_scraper

    def set_cmd(self, cmd):
        self.my_cmd = cmd

    def get_formatter(self):
        return self.my_formatter

    def get_io(self):
        return self.my_IO

    def set_full_dex(self, the_dex):
        self.full_dex = the_dex

    def get_full_dex(self):
        return self.full_dex

    def set_local_dex(self, the_dex):
        self.local_dex = the_dex

    def get_local_dex(self):
        return self.local_dex

    def ui_start(self):
        if len(sys.argv) > 1:
            for i in sys.argv:
                if re.search('-g', i):
                    gen = self.my_IO.get_user_in("Enter the generation you "
                                                 "want to see")
                    if gen.isdigit():
                        self.start(gen, self.default_path, self.defaultName)
        else:
            pass

    def start(self, the_gen, filepath, filename):
        f = self.my_formatter
        x = self.my_IO
        try:
            self.set_full_dex(x.load(filepath, filename))
        except FileNotFoundError:
            self.response = self.my_IO.get_user_in(
                "File not found. Would you like to perform a fresh scrape "
                "'Y or N'")
            self.example(0)
        except EOFError:
            self.response = self.my_IO.get_user_in(
                "Error reading file. Would you like to perform a fresh scrape "
                "'Y or N'")
            self.example(0)
        self.full_dex = self.my_scraper.go()
        x.pickler(self.full_dex, filepath, filename)

        self.my_IO.printer(int(the_gen),
                           f.readability_formatter(self.full_dex))

    def check_user_in(self, prompt):
        if str(prompt).capitalize() == 'Y':
            self.example(0)
        elif str(prompt).capitalize() == 'N':
            self.my_cmd.do_load()

    def example(self, the_gen):
        self.my_scraper.set_generation(the_gen)
        self.set_full_dex(self.my_scraper.go())
