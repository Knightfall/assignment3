from Pokemon import Pokemon
from abc import ABCMeta, abstractmethod


class Builder(metaclass=ABCMeta):

    @abstractmethod
    def build(self, data):
        pass

    @abstractmethod
    def get(self):
        pass


class PokemonBuilder(Builder):
    def __init__(self):
        self.pokemon = Pokemon()

    def build(self, data):
        self.pokemon.set_num(data[0])
        self.pokemon.set_name(data[1])
        self.pokemon.set_type1(data[2])
        self.pokemon.set_type2(data[3])
        self.pokemon.set_url(data[4])
        self.pokemon.set_height(data[5])
        self.pokemon.set_weight(data[6])

    def get(self):
        return self.pokemon
